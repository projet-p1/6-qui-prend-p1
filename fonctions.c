#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <time.h>
#include "projet.h"

void creation_cartes(struct Cartes pile[104]){
  for (int j=0;j<104;j++){
    pile[j].tdb=1;
  }
  for (int i=1;i<=104;i++){
    pile[i].nb=i+1;
    if (pile[i].nb%5==0 && pile[i].nb%10!=0){
      pile[i].tdb=2;
    }
    if (pile[i].nb%10==0){
      pile[i].tdb=3;
    }
    if (pile[i].nb%11==0 && pile[i].nb!=55){
      pile[i].tdb=5;
    }
    if (pile[i].nb==55){
      pile[i].tdb=7;
    }
  }
}

void affiche_cartes(struct Cartes pile[104]){
  for (int i=0;i<104;i++){
    printf("Le nombre %d",pile[i].nb);
    printf(" possede %d tete(s) de taureau(x) \n\n",pile[i].tdb);
  }
}

void shuffle(struct Cartes pile[104]) {
    srand(time(NULL));
    for (int i = 0; i < 104; i++) {
        int j = rand() % 104;
        struct Cartes temp = pile[i];
        pile[i] = pile[j];
        pile[j] = temp;
    }
}
