#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <time.h>
#include "projet.h"


struct Cartes{
  int nb;
  int tdb;
};


int main(int argc, char const *argv[]) {
  int nb_joueurs;
  struct Cartes pile[104],melange[104];
//  printf("Bonjour, combien de joueurs veulent jouer?");
//  scanf("%d",&nb_joueurs);
  creation_cartes(pile);
  for (int i = 0; i < 104; i++) {
        melange[i] = pile[i];
  }
  shuffle(melange);
  printf("Cartes après le mélange:\n");
  affiche_cartes(melange);
  return 0;
}
